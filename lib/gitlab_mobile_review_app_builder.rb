# frozen_string_literal: true

require_relative "gitlab_mobile_review_app_builder/version"
require_relative "gitlab_mobile_review_app_builder/cli"
require 'fastlane'
require 'fastlane/actions/appetize'
require 'http'

module GitlabMobileReviewAppBuilder
  class Error < StandardError; end

  class Android
    def self.deploy_to_appetize!(options)
      token      = options[:token]    || ENV['APPETIZE_API_TOKEN']
      path       = options[:path]     || 'app/build/outputs/apk/debug/app-debug.apk'
      api_host   = options[:api_host] || 'api.appetize.io'

      Fastlane::Actions::AppetizeAction.run(
        path: path,
        api_token: token,
        platform: 'android',
        api_host: api_host
      )

      public_key = Fastlane::Actions.lane_context[Fastlane::Actions::SharedValues::APPETIZE_PUBLIC_KEY]

      File.write('./review.env', "APPETIZE_PUBLIC_KEY=#{public_key}")
    end

    def self.delete_from_appetize!(options)
      token      = options[:token]    || ENV['APPETIZE_API_TOKEN']
      api_host   = options[:api_host] || 'api.appetize.io'

      response = HTTP.basic_auth(user: token, pass: '').delete("https://#{api_host}/v1/apps/#{options[:public_key]}")

      raise "Delete failed: #{response.status}" if response.status != 200
    end
  end
end
