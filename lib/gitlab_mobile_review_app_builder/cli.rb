require 'thor'
module GitlabMobileReviewAppBuilder
  class CLI < Thor
    desc "deploy", "Deploy an APK to appetize"
    option :token
    option :path
    def deploy
      GitlabMobileReviewAppBuilder::Android.deploy_to_appetize!(options)
    end

    desc "destroy", "Destroy an appetize app"
    option :token
    option :public_key
    def destroy
      GitlabMobileReviewAppBuilder::Android.delete_from_appetize!(options)
    end
  end
end
  