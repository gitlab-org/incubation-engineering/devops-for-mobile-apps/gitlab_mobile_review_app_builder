# frozen_string_literal: true

RSpec.describe GitlabMobileReviewAppBuilder do
  it "has a version number" do
    expect(GitlabMobileReviewAppBuilder::VERSION).not_to be nil
  end

  it "does something useful" do
    expect(false).to eq(true)
  end
end
